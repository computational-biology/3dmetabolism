import os
import sys
import shutil
from paths import *
from ranges import *

# Config input variables
cell_type = config["cell_type"]
target_category = config["target_category"]
target_bed = config["target_bed"]
marks_dataset = config["marks_dataset"]
marks_list = config["marks_list"]
LAD_bed = config["LAD_bed"]


# Paths
listpath = os.path.join(marklistspath, marks_list)
sourcemarkspath = os.path.join(allpath, cell_type, marks_dataset)
selmarkspath = os.path.join(selpath, cell_type, marks_dataset)
targpath = os.path.join(targetpath, target_category, target_bed)
resultspath = os.path.join(targpath,marks_dataset + "_results")
LADsgdpath = os.path.join(ladspath, "hi-c", cell_type, LAD_bed)


# Selected marks to analyse (equivalences)
with open(listpath,'r') as l:
    selfiles = {rows.split(";")[1][:-1]:rows.split(";")[0] for rows in l}
l.close()

# Analyced Marks
marks = [m for m in selfiles.keys()]

# WORKFLOW
#---------------------------------------

# Select histone chip-seq
rule selection:
    input:
        [os.path.join(sourcemarkspath, fnam) for fnam in os.listdir(sourcemarkspath)]
    output:
        [os.path.join(selmarkspath, fnam, selfiles[fnam] + ".bed.gz") for fnam in selfiles.keys()]
    run:
        os.system(f"python selection.py {cell_type} {marks_list} {marks_dataset}")


# Filter alternatives intervals regions in files (chr1-22,X,Y)
rule processing:
    input:
        rules.selection.output
    output:
        os.path.join(targpath, target_bed + "_clean.bed"),
        [ os.path.join(selmarkspath, fnam, fnam + ".bed.gz") for fnam in selfiles.keys()]
    run:
        os.system(f"python processing.py {cell_type} {target_category} {target_bed} {marks_dataset}")


# Marks distribution in chromatin, statistics and graphs
rule statistics:
    input:
        rules.processing.output,
        [os.path.join(LADsgdpath, LAD) for LAD in os.listdir(LADsgdpath)]
    output:
        [os.path.join(resultspath, "hic_values", m + "_hic.bed") for m in marks],
        [os.path.join(resultspath, "hic_statistics","tabs", m + "_hic_stats.bed") for m in marks],
        os.path.join(resultspath, "hic_statistics","test", "qvalues_marks.png"),
        [os.path.join(resultspath, "hic_plot",  m + '.png') for m in marks],
        os.path.join(resultspath, "hic_plot", "normalized_marks.png")
    run:
        os.system(f" python hic_statistics.py {cell_type} {target_category} {target_bed} {marks_dataset} {LAD_bed}")
        os.system(f" python hic_tendency.py {cell_type} {target_category} {target_bed} {marks_dataset}")
