import os
import sys
import shutil
from paths import *

# Config input variables
chromatin_category = config["chromatin_category"]
chromatin_bed = config["chromatin_bed"]
marks_dataset = config["marks_dataset"]
marks_list = os.path.join(marklistspath, config["marks_list"])

# If it is the first analysis for a dataset
sel_marks = os.path.join(selpath, marks_dataset)
if not os.path.exists(sel_marks):
    os.makedirs(sel_marks)

# list of selected histone chip-seqs
with open(marks_list,'r') as l:
    selfiles = {rows.split(";")[1][:-1]:rows.split(";")[0] for rows in l}
l.close()


# WORKFLOW
#---------------------------------------

# Select histone chip-seq
rule selection:
    input:
        [ os.path.join(allpath, fnam) for fnam in os.listdir(allpath) ]
    output:
        [ os.path.join(selpath, marks_dataset, fnam.split("-")[0], selfiles[fnam] + ".bed.gz") for fnam in selfiles.keys() ]
    run:
        os.system(f"python selection.py {marks_list} {marks_dataset}")


# Filter alternatives intervals regions in files (chr1-22,X,Y)
rule processing:
    input:
        rules.selection.output
    output:
        os.path.join(chromatinpath, chromatin_category, chromatin_bed, chromatin_bed + "_clean.bed"),
        [ os.path.join(selpath, marks_dataset, fnam.split("-")[0], fnam.split("-")[0] + ".bed.gz") for fnam in selfiles.keys()]
    run:
        os.system(f"python processing.py {chromatin_category} {chromatin_bed} {marks_dataset}")



chromatin_dir = os.path.join(chromatinpath, chromatin_category, chromatin_bed)
marks = os.listdir(os.path.join(selpath,marks_dataset))

# Marks distribution in chromatin, statistics and graphs
rule statistics:
    input:
        rules.processing.output,
        [os.path.join(lamin_b1, 'window', LAD) for LAD in os.listdir(os.path.join(lamin_b1, 'window'))]
    output:
        [os.path.join(chromatin_dir , marks_dataset + "_results", "values", m + "_val.bed") for m in marks],
        [os.path.join(chromatin_dir , marks_dataset + "_results", "statistics", m + "_stats.bed") for m in marks],
        os.path.join(chromatin_dir , marks_dataset + "_results", "plot", "normalized_marks.png")
    run:
        os.system(f" python statistics.py {chromatin_category} {chromatin_bed} {marks_dataset}")
        os.system(f" python tendency.py {chromatin_category} {chromatin_bed} {marks_dataset}")
