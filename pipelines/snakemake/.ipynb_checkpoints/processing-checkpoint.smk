import os
import sys
import shutil
from paths import *


category = config["category"]
bed = config["bed"]
mark_list = config["list"]

sample = os.path.join(chromatin, category, bed)

chip = [i.split(".gz")[0] for i in os.listdir(zip_marks) if i.endswith('.gz')]

with open(mark_list,'r') as l:
    files = {rows.split(";")[1][:-1]:rows.split(";")[0] for rows in l}
l.close()



rule unzip_data:
    input: 
        [ os.path.join(zip_marks, i + ".gz") for i in chip ]
    output: 
        [ os.path.join(all_marks, i) for i in chip ]
    run:
        os.system(f" python unzip_data.py ")
    
    
    
rule selected_beds:
    input: 
        rules.unzip_data.output
    output:
        [ os.path.join(sel_marks, i.split("-")[0], files[i] + ".bed") for i in files.keys() ]
    run:
        os.system(f" python selected_beds.py {mark_list}")


        
rule processing_file:
    input:
        os.path.join(sample, bed + ".bed"),
        rules.selected_beds.output
    output:
        os.path.join(sample, bed + "_clean.bed"),
        [ os.path.join(sel_marks, mark, mark + ".bed") for mark in os.listdir(sel_marks) ]
    run:
        os.system(f" python processing_file.py {category} {bed}")



rule statistics:
    input:
        rules.processing_file.output,
        [os.path.join(lamin_b1, 'window', LAD) for LAD in os.listdir(os.path.join(lamin_b1, 'window'))]
    output:
        [os.path.join(sample, "results", "values", mark + "_val.bed") for mark in os.listdir(sel_marks)],
        [os.path.join(sample, "results", "statistics", mark + "_stats.bed") for mark in os.listdir(sel_marks)],
        os.path.join(sample, 'results', 'plot', 'normalized_marks.png')
    run:
        os.system(f" python statistics.py {category} {bed}")
        os.system(f" python tendency.py {category} {bed}")
             
        