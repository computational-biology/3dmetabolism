import os

# PATHS: dirs
# ------------------------------------------------------------------------------

# Root
wd = os.path.normpath(os.path.join(os.getcwd(), os.pardir))

# target_coordinates
targetpath = os.path.join(wd, 'data', 'target_coordinates')

# Hi-C
hicpath = os.path.join(wd, 'data', 'Hi-C')

# LADs
ladspath = os.path.join(wd, 'data', 'LADs')

# Marks
markspath = os.path.join(wd, 'data', 'histone_marks')
allpath = os.path.join(markspath, 'all')
selpath = os.path.join(markspath,'selected')

# Snakemake
snakemakepath = os.path.join(wd,'pipelines','snakemake')

# List of Samples to be used
marklistspath = os.path.join(wd, 'pipelines', 'marklists')

# Config inputs
configpath = os.path.join(wd,'pipelines','config')
