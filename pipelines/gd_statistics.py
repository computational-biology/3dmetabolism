import os
import sys
import time
import datetime
import pandas as pd
import numpy as np
import pyranges as pr
from paths import *
from ranges import * # LAD_ranges LAD_names

# Param arguments
cell_type = sys.argv[1]
target_category = sys.argv[2]
target_bed = sys.argv[3]
marks_dataset = sys.argv[4]
LAD_bed = sys.argv[5]

# cell_type = "H1-hESC"
# target_category = "genes"
# target_bed = "genes_IDR800_10kb"
# marks_dataset = "Encode"
# LAD_bed = "Lamin_B1"

#Paths
selmarkspath = os.path.join(selpath, cell_type, marks_dataset)
target_dir = os.path.join(targetpath, target_category, target_bed)

# lamin places
labels = [str(LAD_names[ind]) +"-"+str(LAD_names[ind+1]) for ind in range(0,len(LAD_names)-1)]
labels.insert(0, "InsideLAD")
labels.insert(len(labels), "Center")

LAD_names = LAD_names[1:]
LAD_names.insert(0, "InsideLAD")
LAD_names.insert(len(LAD_names), "Center")

intervals = dict(zip(LAD_names, labels))

# Normalization
def div_to_val(df, **kwargs):
    col = kwargs['divisor']
    df.loc[:, 'Val'] = df['Val'].div(df[col], axis = 0)
    return df


# Get marks path
marcas = {}
for rep in os.listdir(os.path.join(selmarkspath)):
    marcas[rep] = []
    for fnam in os.listdir(os.path.join(selmarkspath, rep)):
        if not fnam.startswith('H'):
            continue
        marcas[rep].append(os.path.join(selmarkspath, rep, fnam))


# Open target bed:
for fnam in os.listdir(target_dir):
    if fnam.endswith('_clean.bed'):
        file = os.path.join(target_dir, fnam)
        target_gr = pr.read_bed(file)


# Get ovelapped intervals and normalization by ranges or distance
val_marks = {}
for mark in marcas.keys():
    for fnam in marcas[mark]:
        mark_df = pr.read_bed(fnam)
        gr = target_gr.count_overlaps(mark_df, overlap_col="Val")
        if target_category == "genes":
            val_marks[mark] = gr.apply(div_to_val, divisor='Score')
            cols = ['Chromosome', 'Start','End','Genes','Exp','Val','Lamin']
        else:
            gr.Lengths = gr.lengths()
            val_marks[mark] = gr.apply(div_to_val, divisor='Lengths')
            cols = ['Chromosome', 'Start', 'End', 'Val', 'Lengths', 'Lamin']


# This block is dedicated to the construction of a new chromatin bed with all their intervals
# classified according to the place occupated in chromatin respectively to Lamin B1 and the numbers
# of histone marks loceted in them for each mark.
range_LADs = {}
for mark in marcas.keys():

    range_LADs[mark] = { i : [] for i in LAD_names }

    for ind, win in enumerate(LAD_names):

        results = os.path.join(target_dir, marks_dataset + "_results")
        val_path = os.path.join(results, 'values')
        stat_path = os.path.join(results, 'statistics', 'tabs')
        test_path = os.path.join(results, 'statistics', 'test')

        # Create Results folders
        if not os.path.exists(results):
            for dir in [results, val_path, stat_path, test_path]:
                os.makedirs(dir)

        # Center is the last step of the analysis
        if win != 'Center':
            fnam_LAD = os.path.join(ladspath, "gd", cell_type, LAD_bed, LAD_bed + "." + win + ".bed")

            LAD_df = pr.read_bed(fnam_LAD)
            range_LADs[mark][win] = {'LAD':[], 'noLAD':[]}

            # Marks which ovelapps current chromatin region in each space determinated by the distance to Lamin

            ## Initial stating range
            if win == 'InsideLAD':
                print("mark: " + mark + " LAD range: " + win)
                gr = val_marks[mark].count_overlaps(LAD_df,
                                                    overlap_col="Lamin_count")
                df = gr.df
                df['Lamin'] = "noLAD"

            ## Nexts ranges towards the center
            else:
                print("mark: " + mark + " LAD range: " + win)
                gr = pr.PyRanges(range_LADs[mark][LAD_names[ind-1]]['noLAD'])
                gr = gr.count_overlaps(LAD_df, overlap_col="Lamin_count")
                df = gr.df


            # Filtering those chromatin region whitn al least one mark in the region that we are checking
            df.loc[df['Lamin_count'] > 0, 'Lamin'] = intervals[win]
            df = df.drop('Lamin_count', axis=1)

            # Reset index of dataframe
            noLAD = df.loc[df['Lamin'] =="noLAD"].reset_index(drop=True)
            LAD = df.loc[df['Lamin'] ==intervals[win]].reset_index(drop=True)

            # Save the subdataframe for each lamin place and the rest of intervals to be re-checked
            # for other lamin places
            range_LADs[mark][win]['noLAD'] = noLAD
            range_LADs[mark][win]['LAD'] = LAD

        else:
            print("mark: " + mark + " LAD range: " + win)
            df = range_LADs[mark][LAD_names[ind-1]]['noLAD']
            df['Lamin'] = win
            range_LADs[mark][win] = df


# Concat all subdataframes in a unique datrame with al the classfied chromatin regions
for mark in marcas.keys():
    if target_category == 'genes':
        dfVal = pd.DataFrame(columns=['Chromosome', 'Start', 'End',
                                      'Genes', 'Exp', 'Val', 'Lamin'])
    else:
        dfVal= pd.DataFrame(columns=['Chromosome', 'Start', 'End',
                                      'Val', 'Lengths', 'Lamin'])

    dfStat = pd.DataFrame(columns=['Mark', 'Lamin', 'Mean', 'Std'])

    for win in LAD_names:
        if win != 'Center':
            df = range_LADs[mark][win]['LAD']
            df.columns = cols
            dfVal = pd.concat([dfVal,df])
        else:
            df = range_LADs[mark][win]
            df.columns = cols
            dfVal = pd.concat([dfVal,df])

    # Statistics: mean and std by lamin space
    # pd.set_option('display.float_format', lambda x: '%.5f' % x)
    with open(os.path.join(stat_path, mark + "_stats.bed"),'w') as f:
        for win in LAD_names:
            val_by_range = dfVal.loc[dfVal['Lamin'] == intervals[win], 'Val']
            mean_by_range = val_by_range.mean()
            std_by_range = val_by_range.std()

            print(mark, win, "mean: " + str(mean_by_range), "std: " + str(std_by_range))
            f.write('{}\t{}\t{}\t{}\n'.format(mark, intervals[win], mean_by_range, std_by_range))

    # Sort values and reset the dataframe index
    dfVal= dfVal.sort_values(['Chromosome', 'Start', 'End'],
                        ascending=True).reset_index(drop = True)

    # Save statistics
    dfVal.to_csv(os.path.join(val_path, mark + "_val.bed"),
                header=False, index= False,  sep='\t')
