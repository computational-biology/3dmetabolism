import os
import sys
import numpy as np
import pandas as pd
from paths import *
from ranges import * # LAD_ranges LAD_names
import scipy.stats as stats
import dataframe_image as dfi
from matplotlib import pyplot as plt
from statsmodels.stats.multitest import multipletests

cell_type = sys.argv[1]
target_category = sys.argv[2]
target_bed = sys.argv[3]
marks_dataset = sys.argv[4]


# Statistical test
Test = 'mannwhitneyu'

# TAB STYLE CSS

# Table styles
th_props = [
  ('font-size', '11px'),
  ('text-align', 'center'),
  ('font-weight', 'bold'),
  ('color', '#6d6d6d'),
  ('background-color', '#f7f7f9')
  ]

# Set CSS properties for td elements in dataframe
td_props = [
  ('font-size', '11px'),
  ('text-align', 'center'),
  ('background-color', '#ffffff')
  ]

# Set table styles
styles = [
  dict(selector="th", props=th_props),
  dict(selector="td", props=td_props)
  ]


# Remark significance test by positive/negative tendency
def _HighlighTendency(df):
    c1 = 'color:red'
    c2 = 'color:blue'
    df1 = pd.DataFrame('', index=df.index, columns=df.columns)
    for m in marks:
        if m in list(df.index):
            s = pvalues.loc[pvalues['marks'] == m, 'tend'].tolist()
            df1.loc[[m],:] = [c1 if t=='+' else c2 for t in s]
    return df1


# Check normal distribution and calculation of
def statistical_tests(samp1, samp2, test):
    # Check normal distribution (parametric/not parametric)
    s_kolmog, p_kolmog = stats.kstest(samp1,'norm')
    s_kolmog2, p_kolmog2 = stats.kstest(samp2,'norm')

    # Not parametric test if at least one is not parametric
    if p_kolmog <= 0.05 or p_kolmog2 <= 0.05:
        if all(samp1 == 0.0) and all(samp2 == 0.0):
            p = 1
        else:
            if test == 'mannwhitneyu':
                s, p = stats.mannwhitneyu(samp1,samp2)
    else:
        raise Exception("Normal distribution")

    return p



# Lamin places
order = [str(LAD_names[ind]) +"-"+ str(LAD_names[ind+1]) for ind in range(0,len(LAD_names)-1)]
order.insert(0, "InsideLAD")
order.insert(len(order), "Center")


# Save column tags and paris of orders of ranges to test
columns = []
pairs = []
for ind in range(0,len(order)):
    if ind+1 == len(order):
       columns.append(order[0]+'<br>'+order[ind])
       pairs.append((order[0],order[ind]))
    else:
        columns.append(order[ind]+'<br>'+order[ind+1])
        pairs.append((order[ind],order[ind+1]))

# Paths
target_dir = os.path.join(targetpath, target_category, target_bed)
results_path = os.path.join(target_dir, marks_dataset + '_results', 'values')
marks = [m.rsplit('_', 1)[0] for m in os.listdir(results_path)]
num_ac = len([m for m in marks if "ac" in m])
num_met = len([m for m in marks if "me" in m])


NormData = {}
for w in order:
    for mark in marks:
        mark_path = os.path.join(results_path, mark + "_val.bed")
        dfVal = pd.read_csv(mark_path, sep = "\t", header=None)

        if target_category == "genes":
            dfVal.columns = ['Chromosomes','Start','End', 'Genes',
                             'Exp', 'Val', 'Lamin']
        else:
            dfVal.columns = ['Chromosomes','Start','End',
                             'Val','Lengths','Lamin']

        # Normalization of histone marks number.
        # Dvide the number of marks in each bed interval by the sum of all marks in all lamin places
        total = dfVal['Val'].sum()
        dfVal['Val'] = dfVal['Val'].div(total) * 1000

        # Save by lamin ranges for each mark
        NormData[mark] = {}
        for rang in order:
            NormData[mark][rang] = dfVal.loc[dfVal['Lamin'] == rang,'Val']


# Preparations of inputs
dicMarks = {}
pvalues = pd.DataFrame(columns=['marks', 'pval', 'tend'])
for nm, mark in enumerate(marks):
    tendency = []
    markpvalues = []
    marcas = []
    for ind, rang in enumerate(order):
        if ind+1 == len(order):
            rang1, rang2 = order[0], rang
        else:
            rang1, rang2 = rang, order[ind+1]

        # Pair of samples to compare
        sample1 = NormData[mark][rang1]
        sample2 = NormData[mark][rang2]

        # Apply statistic tests
        markpvalues.append(statistical_tests(sample1,sample2, Test))
        marcas.append(mark)
        # Tendency by lamin range by mark
        t =  NormData[mark][rang2].mean() - NormData[mark][rang1].mean()
        if t > 0:
            tendency.append('+')
        else:
            tendency.append('-')

    # All pvalues in each pair by mark
    pvalues = pd.concat([pvalues, pd.DataFrame.from_dict({'marks': marcas, 'pval':markpvalues, 'tend': tendency})])

# Apply multipletesting correction: Benjamin/Hotchberg
# Tendency = > (+) increasing, (-) decreasing
# Significance = > (***) 0.001, (**) 0.01, (*) 0.05
pvalues['pval_adj'] = list(multipletests(pvalues['pval'].tolist(), method = "fdr_bh")[1])
pvalues['pval_adj'] = pvalues['pval_adj'].apply(lambda x: '***' if x <= 0.001 else
                                                          '**' if  x <= 0.01 else
                                                          '*' if x <= 0.05 else '')
pvalues['labels'] = pvalues['tend'] * pvalues['pval_adj'].str.len()


# Table of marks significance/tendency
for m in marks:
    dicMarks[m] = pvalues.loc[pvalues['marks'] == m, 'labels'].tolist()
dfMarks = pd.DataFrame.from_dict(dicMarks, orient = 'index')
dfMarks.columns = columns

# Order Marks
orderMarks = []
for num, what in enumerate(['Acethylation', 'Methylation'], 1):
    orderMarks.extend(sorted([m for m in dicMarks.keys() if what[:2].lower() in m]))

dfMarks = dfMarks.reindex(orderMarks)


# Save statistic tab
tab_result = os.path.join(target_dir, marks_dataset + "_results", 'statistics', 'test')
dfMarks_styled = dfMarks.style.apply(_HighlighTendency, axis = None).set_table_styles(styles)
dfi.export(dfMarks_styled, os.path.join(tab_result, "qvalues_marks.png"))

# General plot tendency
plt.figure(figsize=(12, 12))
for num, what in enumerate(['Acethylation', 'Methylation'], 1):
    axe = plt.subplot(2,1,num)
    ys = []
    these_marks = sorted([m for m in marks if what[:2].lower() in m],
                         key=lambda x: np.mean(NormData[x][order[-1]]))
    for n, m in enumerate(these_marks):

        tendency = np.mean(NormData[m][order[-1]]) - np.mean(NormData[m][order[0]])
        if tendency <= 0:
            tendency = 0

        plt.plot(range(len(order)),
                 [np.mean(NormData[m][w]) for w in order], 'o-',
                 alpha=min(1, 0.1 + 1 * tendency),
                 color='k', lw=3, zorder=1000)
        y = np.mean(NormData[m][order[-1]])
        if target_category == 'genes':
            y2 = 0.05 + 0.8 * n / len(these_marks)
            plt.ylim(0, 0.9)
        else:
            y2 = 0.05 + 7 * n / len(these_marks)
            #plt.ylim(0, 6)
        plt.text(len(order) - 0.4, y2, m, va='center')
        plt.plot([len(order) - 0.9, len(order) - 0.4], [y, y2], 'k:', lw=1)


    _ = plt.xticks(range(len(order)), order)
    ylim = plt.ylim()
    plt.plot([0, len(order) - 1], [0] * 2, 'k-')
    plt.xlim(plt.xlim()[0], len(order) + 2)
    #plt.title(what)
    #plt.xlabel(what,fontsize=12)
    if target_category == "genes":
        plt.ylabel('Normalized histone marks per CAGE-seq read',fontsize=12, labelpad=10)
    else:
        plt.ylabel('Normalized histone marks per length of RIs',fontsize=12, labelpad=10)
    plt.xlabel('Division of compartments in the nucleus',fontsize=12, labelpad=10)
    axe.xaxis.set_label_coords(0.32,-0.2)
    #fig.suptitle('test title', fontsize=20)
    axe.spines['right'].set_visible(False)
    axe.spines['top'].set_visible(False)
    axe.spines['bottom'].set_visible(False)
    plt.xticks(rotation=15, fontsize = 12)

plot_path = os.path.join(target_dir, marks_dataset + '_results', 'plot')
if not os.path.exists(plot_path):
    os.makedirs(plot_path)

print("Generating general plot")
plt.savefig(os.path.join(plot_path, 'normalized_marks.png'), dpi=300)
plt.close()



# Mark plot tendency (mark by mark)
for num, what in enumerate(['Acethylation', 'Methylation'], 1):

    these_marks = sorted([m for m in marks if what[:2].lower() in m],
                         key=lambda x: np.mean(NormData[x][order[-1]]))

    for n, m in enumerate(these_marks):

        plt.figure(figsize=(8, 8))

        tendency = np.mean(NormData[m][order[-1]]) - np.mean(NormData[m][order[0]])
        if tendency <= 0:
            tendency = 0

        axe = plt.subplot(1,1,1)
        plt.plot(range(len(order)),
                 [np.mean(NormData[m][w]) for w in order], 'o-',
                 alpha=min(1, 0.1 + 1 * tendency),
                 color='k', lw=3, zorder=1000)

        _ = plt.xticks(range(len(order)), order)
        ylim = plt.ylim()
        plt.plot([0, len(order) - 1], [0] * 2, 'k-')
        plt.title(str(what + "("+ m +")"))
        if target_category == "genes":
            plt.ylabel('Normalized histone marks per CAGE-seq read',fontsize=14, labelpad=10)
        else:
            plt.ylabel('Normalized histone marks per length of RIs',fontsize=14 , labelpad=10)
        plt.xlabel('Division of compartments in the nucleus',fontsize=14, labelpad=10)
        axe.spines['right'].set_visible(False)
        axe.spines['top'].set_visible(False)
        axe.spines['bottom'].set_visible(False)
        plt.xticks(rotation=10, fontsize = 12)
        plt.tight_layout()

        print("Generating plot: " + m)
        plt.savefig(os.path.join(plot_path, m + '.png'), dpi=300)
        plt.close()
print("Process finished")
