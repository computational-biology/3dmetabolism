

# DATASETS 

## Current dataset:

#### ENCODE: Encyclopedia of DNA Elements (https://www.encodeproject.org/)
<hr>
**Histone Chip-seq (hESC-H1):** H2AK5ac, H2BK120ac, H2BK12ac, H2BK15ac, H2BK20ac, H2BK5ac, H3K14ac,  H3K18ac, H3K23ac, H3K23me2,  H3K27ac,  H3K27me3,  H3K36me3,  H3K4ac, H3K4me1, H3K4me2,  H3K4me3, H3K56ac, H3K79me1, H3K79me2, H3K9ac, H3K9me3, H4K20me1, H4K5ac, H4K8ac, H4K91ac

**Proyect:** UCSD Human Reference Epigenome Mapping Project<br>
**Reference:** https://www.ncbi.nlm.nih.gov/geo/roadmap/epigenomics/?view=studies

**Download:** 
```
xargs -L 1 curl -O -J -L < files.txt
```


## Alternative datasets:

#### 4D Nucleome Data Portal (https://data.4dnucleome.org/)
<hr>
**Sample:** in situ Hi-C on H1-hESC (Tier 1) with DpnII

**Publication:** <b><span style="color:#1f8be0">Ultrastructural Details of Mammalian Chromosome Architecture</span></b><br>
**Reference:** https://pubmed.ncbi.nlm.nih.gov/32213324/

**Download:**

https://data.4dnucleome.org/experiment-set-replicates/4DNES2M5JIGV/#raw-files



#### Gene Expression Omnibus (https://www.ncbi.nlm.nih.gov/geo/)
<hr>
**Publication:** <b><span style="color:#1f8be0">TFIIIC Binding to Alu Elements Controls Gene Expression via Chromatin Looping and Histone Acetylation</span></b>

**Why is interesting?** 

- Chip-seq: 2 acetylation marks (H3K9ac, H3K27), other franscription factor (TFIIIC, BDP1, POL3) in different cells
- Hi-C: multiple replicates
- mRNA: in different time points
- ATAC-seq
- Serum starvation/basal conditions en cells
- In Homo-sapiens
- Problem: low number of histone marks, not hESC cells

**Reference:** https://www.ncbi.nlm.nih.gov/pmc/articles/PMC7014570/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE120162


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Chromatin structure dynamics during human cardiomyocyte differention reveals a role of HERV-H in demarcating TAD boundaries</span></b>

**Why is interesting?** 

- transgenic H9 hESC line expressing a H2B-GFP fusion protein (if only is transgenic for GFP mabye it is possible repeat our results with H9 chip-seqs in Encode repository instead of H1)
- HiC, ATAC-seq, RNA-seq, and ChIP-seq for H3K27ac, H3K27me3, H3K4me1, H3K4me3, H3K9me3 and CTCF for every time point, each with two biological replicates. 
- six critical time points during differentiation: human embryonic stem cells (hESC) (Day 0), hESC mesodermal cells (Day 2), hESC-cardiac mesodermal cells (Day 5), hESC-cardiac progenitor cell (Day 7), hESC-primitive cardiomyocytes (Day 15) and hESC-ventricular cardiomyocytes (Day 80)
- In Homo-sapiens
- Problem: no nutrient state, low number of histone marks


**Reference:** https://pubmed.ncbi.nlm.nih.gov/31427791/
           https://pubmed.ncbi.nlm.nih.gov/32111823/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE116862


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Transcriptional and epigenetic profiling of nutrient-deprived cells to identify novel regulators of autophagy</span></b>

**Why is interesting?** 

- 3 epigenetic marks (H3K4me3, H3K27ac, and H3K56ac) 
- Nutrient (amino acid and serum) starvation/normal condition
- Related with macroautophagy: lysosomal degradation pathway critical for maintaining cellular homeostasis and viability, and is predominantly regarded as a rapid and dynamic cytoplasmic process
- Comparation of starving state between 
- In Homo-sapiens
- Problem: no nutrient state, no time points, transcription data, not hESC cells


**Reference:** https://pubmed.ncbi.nlm.nih.gov/30153076/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE107603


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Dynamic regulation of histone modifications and long-range chromosomal interactions during postmitotic transcriptional reactivation</span></b>

**Why is interesting?** 

- 3 epigenetic marks (H3K4me1, H3K27ac, and H3K4me3) and a transcription factor (CTCF) in two cell lines (U2OS, RPE1) in different phases of cell cycle.
- RNA-seq and Hi-C in different time points.
- In Homo-sapiens.
- Problem: no nutrient state, low number of histone marks, not hESC cells


**Reference:** https://pubmed.ncbi.nlm.nih.gov/32499403/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE141139



<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Chromatin Architecture Reorganization during Stem Cell Differentiation</span></b>
    
**Why is interesting?** 

- Multiple replicates for Hi-C and 4C seq in H1 in different H1 hESC tissues
- Re-analysis of data from GSE16256 in an allele specific manner is linked as supplementary data.
- In Homo-sapiens
- Problem: no nutrient state, no time points, transcription data

**Reference:** https://www.ncbi.nlm.nih.gov/pmc/articles/PMC4515363/#SD1

**Download:**

    GSE52457 https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE52457
    GSE16256 (re-analysis) https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE16256



<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Dynamic chromatin states in human ES cells reveal potential regulatory sequences and genes involved in pluripotency</span></b>

**Why is interesting?** 

- 2 epigenetic marks (H3K4me1, H3K27ac) and CTCF transcription factor
- Mapped epigenetic differences in undifferentiated ES cell (hESC) and differentiated ES cell (denoted as DFC) genomes
- ChIP for promoter and enhancer histone modifications were conducted relative to input DNA in hESCs and hESCs treated with BMP4 (bone morphogenetic protein)
- In Homo-sapiens.
- Problem: no  nutrient state, Hi-C, transcription data


**Reference:** https://pubmed.ncbi.nlm.nih.gov/21876557/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE30434


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Functional organization of the human 4D Nucleome</span></b>

**Why is interesting?** 

- Fibroblast.
- Hi-C and RNA-seq in different time points
- In Homo-sapiens.
- Problem: no time points, nutrient state, histone chip-seq

**Reference:** https://pubmed.ncbi.nlm.nih.gov/26080430/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE81087


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">The NIH Roadmap Epigenomics Mapping Consortium</span></b>

**Why is interesting?** 

- Epigenomic map from different cell lines and tissues (includin hESCs)
- In homo-sapiens
- Problem: no time points, nutrient state, Hi-C, transcription data

(* check if our original H1 data is stored here or another repository in GEO)

**Reference:** https://www.ncbi.nlm.nih.gov/pmc/articles/PMC3607281/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE19465


<hr style="width:50%;text-align:left;margin-left:0">

**Publication:** <b><span style="color:#1f8be0">Topologically-associating domains are stable units of replication-timing regulation</span></b>

**Why is interesting?** 

- Multiple replicates for each experiment
- 1 epigenomic mark (H3K27) and otehr transcription factors
- Starved/normal conditions
- 2 time points of 30 min
- Problems: mus-moluscus and only one mark

**Reference:**https://www.ncbi.nlm.nih.gov/pmc/articles/pmid/25409831/

**Download:**

https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE100835