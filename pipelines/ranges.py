## gd analysis

#   Besides the orginal bed with Lamin B1 position,
#   these parammeters limits de genomic distance to lamin of each range

LAD_ranges = [0, 250000, 1000000, 2500000, 5000000]
LAD_names = ["0kb", "250kb", "1000kb", "2500kb", "5000kb"]

#   e.i.:
#
#   ["0kb", "250kb", "1000kb", "2500kb", "5000kb"]   ->  ["InsideLAD", "0kb-250kb", "250kb-1000kb", "1000kb-2500kb", "2500kb-5000kb", "Center"]
#
#   InsideLAD category and center will be added in analysis


## Hi-C analysis

#   LADs coordinates had to be normalicez in order to have comparable bins

norm = 100000

#   Hi-C compartimentations parameters ranges => percentile(data, [x for x in range(stat, stop, step)])

start = 10
stop = 100
step = 10
