import os
import sys
import glob
import shutil
from paths import *


# Paramas from snakefile
cell_type = sys.argv[1]
mark_list = sys.argv[2]
marks_dataset = sys.argv[3]

# Paths
all_marks = os.path.join(allpath, cell_type, marks_dataset)
sel_marks = os.path.join(selpath, cell_type, marks_dataset)
list = os.path.join(marklistspath, mark_list)

## Clean and prepare new samples to be analyced
# Remove samples from previous analysis
if len(os.listdir(sel_marks))!= 0:
    for fnam in os.listdir(sel_marks):
        rm = os.path.join(sel_marks, fnam)
        shutil.rmtree(rm)

# Create empty folders
if not os.path.exists(sel_marks):
    os.makedirs(sel_marks)


## Copy all mark files in marklist in selected
with open(list,'r') as l:

    # Extract data from selected marks
    for n, rows in enumerate(l):
        columns = rows.split(";")
        ID = columns[0]
        mark = columns[1][:-1]

        # All unzip beds
        source = glob.glob(os.path.join(all_marks, ID + '*'))[0]

        # Clasification by mark
        if source.endswith(".gz"):
            destination = os.path.join(sel_marks, mark, ID + ".bed.gz")
        else:
            destination = os.path.join(sel_marks, mark, ID + ".bed")

        # Create marks folders
        if not os.path.isdir(os.path.dirname(destination)):
             os.makedirs(os.path.dirname(destination))

        #Copy selected beds from sample list
        print("copying ", n, " selected file: ", ID + " for mark: ", mark, " to selected")
        shutil.copy(source, destination)
    l.close()
