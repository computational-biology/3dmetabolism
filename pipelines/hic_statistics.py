import os
import sys
import math
import warnings
import pandas as pd
import numpy as np
import pyranges as pr
from paths import *
from ranges import *
from numpy import percentile
from numpy.random import rand
from pandas.core.common import SettingWithCopyWarning

warnings.simplefilter(action="ignore", category=SettingWithCopyWarning)

# Param arguments
cell_type = sys.argv[1]
target_category = sys.argv[2]
target_bed = sys.argv[3]
marks_dataset = sys.argv[4]
LAD_bed = sys.argv[5]



# Open target bed:
selmarkspath = os.path.join(selpath, cell_type, marks_dataset)
target_dir = os.path.join(targetpath, target_category, target_bed)

# Normalization
def div_to_val(df, **kwargs):
    col = kwargs['divisor']
    df.loc[:, 'Val'] = df['Val'].div(df[col], axis = 0)
    return df


# Get marks path
marcas = {}
for rep in os.listdir(os.path.join(selmarkspath)):
    marcas[rep] = []
    for fnam in os.listdir(os.path.join(selmarkspath, rep)):
        if not fnam.startswith('H'):
            continue
        marcas[rep].append(os.path.join(selmarkspath, rep, fnam))


for fnam in os.listdir(target_dir):
    if fnam.endswith('_clean.bed'):
        file = os.path.join(target_dir, fnam)
        target_df = pr.read_bed(file, as_df = True)


# Convert Start site from target bed into Hi-C mesure
# Chr and Start site coordinates asociated to target bed
if target_category == "genes":
    target_df['Start2'] = target_df['Start'].apply(lambda x: x / norm)
    target_df['Start2'] = target_df['Start2'].apply(lambda x: math.trunc(x))
    chrom_init = target_df[['Chromosome', 'Start2', 'Name']]
else:
    target_df['Name'] = ["fragment_" + str(i) for i in range(0,target_df.shape[0])]
    target_df['Start2'] = target_df['Start'].apply(lambda x: x / norm)
    target_df['Start2'] = target_df['Start2'].apply(lambda x: math.trunc(x))
    chrom_init = target_df[['Chromosome', 'Start2', 'Name']]


# Read Hi-C files by chromosome of sample
column_names = ['Chromosome','Bin1', 'Bin2', 'Interactions',
                'LAD1', 'LAD2', 'Target1', 'Target2']
df_comb = pd.DataFrame(columns = column_names)
hic = os.path.join(hicpath,cell_type,"Rando_H1")
for fnam in os.listdir(hic):
    if fnam == os.path.basename(hic) + ".abc":
        continue
    hic_path = os.path.join(hic, fnam)
    hic_df = pd.read_csv(hic_path, sep = "\t", header=None, skiprows=3,
                usecols=[0,1,3], names=['Bin1', 'Bin2', 'Interactions'],
                dtype={0:int, 1:int, 2:float})

    # Extension of dataframe
    # Check correpondence of each pair of bins with LADs regiones or target regions
    hic_df["LAD1"] = 0
    hic_df["LAD2"] = 0
    hic_df["Target1"] = 0
    hic_df["Target2"] = 0

    # Add chromosome location to each file
    chr = fnam.split("_")[1]
    hic_df.insert (0, "Chromosome", chr)
    print("Finding overlaps in chromosome:" + chr)

    # Get position of all posible Bins for the chromosome
    contacts = hic_df['Bin1'].unique()
    gr = pr.from_dict({"Chromosome": chr, "Start": contacts,
                       "End": contacts})

    # Get bed normaliced
    hic_LAD = os.path.join(ladspath,'hi-c',cell_type)
    binLAD = os.path.join(hic_LAD, LAD_bed, LAD_bed)
    LAD_gr = pr.read_bed(binLAD + ".InsideLAD.bed")

    # Overlaps between LAD regions and all Hi-C bins
    gr = gr.count_overlaps(LAD_gr, overlap_col="LAD")
    df = gr.df

    # Get all bins whith contains LAD regions in chromosome
    ladregions = df.loc[df['LAD'] == 1, ['Start']]
    listLAD = ladregions['Start'].tolist()

    # Check which are in extended Hi-C table for each bin combination
    lad_hic = hic_df[(hic_df['Bin1'].isin(listLAD)) | (hic_df['Bin2'].isin(listLAD))]
    # If LADs positions are found, sum 1 in cols related to LAD for each bin
    lad_hic.loc[lad_hic['Bin1'].isin(listLAD), 'LAD1'] = lad_hic.LAD1 + 1
    lad_hic.loc[lad_hic['Bin2'].isin(listLAD), 'LAD2'] = lad_hic.LAD2 + 1

    # Get postion of target bed in checked chromosome
    init = chrom_init.loc[chrom_init['Chromosome'] == chr, ['Start2','Name']]
    # Coordinates dictionary
    gene_dict = dict(zip(init.Start2, init.Name))

    # Add genes in those regions which are coincident
    lad_hic['Target1'] = lad_hic['Bin1'].map(gene_dict)
    lad_hic['Target2'] = lad_hic['Bin2'].map(gene_dict)

    # Concat all chromosomes analysis
    df_comb = pd.concat([df_comb, lad_hic])

# Selecion of those LADs in contacto with Target
df_comb = df_comb.fillna(0)
not_rep = df_comb
lad_target1 = not_rep.loc[(not_rep['LAD1']==1) & (not_rep['Target2']!=0),]
lad_target2 = not_rep.loc[(not_rep['LAD2']==1) & (not_rep['Target1']!=0),]
def_df = pd.concat([lad_target1, lad_target2]).reset_index()

# Get max interation for Target 1 and Target 2
tar_int1 = def_df.loc[:,["Target1", "Interactions"]]
tar_int1.columns = ['Name', 'Interactions']
grouped_df1 = tar_int1.groupby("Name")
grouped_df1 = grouped_df1
maximums1 = grouped_df1.max()
maximums1 = maximums1.reset_index()

tar_int2 = def_df.loc[:,["Target2", "Interactions"]]
tar_int2.columns = ['Name', 'Interactions']
grouped_df2 = tar_int2.groupby("Name")
grouped_df2 = grouped_df2
maximums2 = grouped_df2.max()
maximums2 = maximums2.reset_index()

# Combinación de las interacciones
tar_int = pd.merge(maximums1,maximums2, on="Name", how="outer").fillna(0)

# Max posible interation for target regions
tar_int["Interactions"] = tar_int[["Interactions_x", "Interactions_y"]].max(axis=1)
Genetab = tar_int.loc[tar_int['Name']!=0,['Name','Interactions']]

Genetab = pd.merge(Genetab,target_df)

# Choose compartiments to cells
data = Genetab.Interactions
media = data.mean()
quartiles = percentile(data, [x for x in range(start, stop, step)])
data_min, data_max = data.min(), data.max()
perc_list = [round(i,3) for i in quartiles]
print('Percentiles:', perc_list)
print('Mean: %.3f' % media)
print('Min: %.3f' % data_min)
print('Max: %.3f' % data_max)

# Establish caterogires of compartiments dividin all interaccions in similare groups
Center = perc_list[0]
InsideLAD = perc_list[len(perc_list)-1]

Genetab['Lamin'] = ""
Genetab.loc[Genetab['Interactions'] < Center, 'Lamin'] = "Center (" + str(Center) + ")"
Genetab.loc[(Genetab['Interactions'] >= InsideLAD), 'Lamin'] = "InsideLAD (" + str(InsideLAD) + ")"

for i in range(0,len(perc_list)-1):
    Genetab.loc[(Genetab['Interactions'] >= perc_list[i]) & (Genetab['Interactions'] < perc_list[i+1]), 'Lamin'] = str(perc_list[i]) + " - " + str(perc_list[i+1])

percentile_list = []
percentile_list.append("Center (" + str(perc_list[0]) + ")")
for i in range(0,len(perc_list)-1):
    percentile_list.append(str(perc_list[i]) + " - " + str(perc_list[i+1]))
percentile_list.append("InsideLAD (" + str(perc_list[len(perc_list)-1]) + ")")

# # Change Hi-C ranges
# text = "hic_order = "   # if any line contains this text, I want to modify the whole line.
# new_text = "hic_order = " + str(percentile_list)
# pathRanges = "/home/bscuser/3DMetabolism/pipelines/ranges.py"
# with open(pathRanges,"r") as f:
#     for line in f.readlines():
#         if text in line:
#             line = new_text
#             print(line)
#     f.close()


# Paths
results = os.path.join(target_dir, marks_dataset + "_results")
valpath = os.path.join(results, 'hic_values')
statpath = os.path.join(results, 'hic_statistics', 'tabs')
testpath = os.path.join(results, 'hic_statistics', 'test')
plotpath = os.path.join(results, 'hic_plot')

# Create Results folders
if not os.path.exists(results):
    for dir in [results, valpath, statpath,testpath, plotpath]:
        os.makedirs(dir)
else:
    if not os.path.exists(valpath):
        for dir in [valpath, statpath, testpath, plotpath]:
            os.makedirs(dir)

# Get ovelapped intervals and normalization by ranges or distance
val_marks = {}
for mark in marcas.keys():
    for fnam in marcas[mark]:
        mark_df = pr.read_bed(fnam)
        Genetab_gr = pr.PyRanges(Genetab)
        gr = Genetab_gr.count_overlaps(mark_df, overlap_col="Val")
        if target_category == "genes":
            val_marks[mark] = gr.apply(div_to_val, divisor='Score')
            cols = ['Chromosome', 'Start','End','Genes','Exp','Val','Lamin']
        else:
            gr.Lengths = gr.lengths()
            val_marks[mark] = gr.apply(div_to_val, divisor='Lengths')
            cols = ['Chromosome', 'Start', 'End', 'Val', 'Lengths', 'Lamin']


    # Statistics: mean and std by lamin space
    # pd.set_option('display.float_format', lambda x: '%.5f' % x)
    with open(os.path.join(statpath, mark + "_hic_stats.bed"),'w') as file:
        dfVal = val_marks[mark].df
        for perc in percentile_list:
            val_by_range = dfVal.loc[dfVal['Lamin'] == perc, 'Val']
            mean_by_range = val_by_range.mean()
            std_by_range = val_by_range.std()

            print(mark, perc, "mean: " + str(mean_by_range), "std: " + str(std_by_range))
            file.write('{}\t{}\t{}\t{}\n'.format(mark, perc, mean_by_range, std_by_range))

    # Save values
    dfVal.to_csv(os.path.join(valpath, mark + "_hic.bed"),
                header=False, index= False,  sep='\t')
