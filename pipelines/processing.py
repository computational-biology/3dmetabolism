import os
import sys
import pandas as pd
import numpy as np
import pyranges as pr
from paths import *
from natsort import natsorted

# Paramas from snakefile
cell_type = sys.argv[1]
target_category = sys.argv[2]
target_bed = sys.argv[3]
marks_dataset = sys.argv[4]


print("Selected category:", target_category, " and selected bed:", target_bed)

# Create list to filter
chr_list = ['chr' + str(i) for i in range(1,23)]
chr_list.append('chrX')
chr_list.append('chrY')


## Target coordenates beds

target = os.path.join(targetpath, target_category,
                      target_bed, target_bed + ".bed")

# By default, the loaded bed only have three chromosome, start, end
if target_category == "genes":
    target_df = pd.read_csv(target, sep = "\t", header=None, usecols=range(5),
                   names=['Chromosome', 'Start', 'End', 'Genes', 'Exp'])
else:
    target_df = pd.read_csv(target, sep = "\t", header=None, usecols=range(3),
                   names=['Chromosome', 'Start', 'End'])

print("Filtering: " + target_bed + ".bed")

# Remove alternative elements
target_df = target_df[target_df['Chromosome'].isin(chr_list)]
target_df.to_csv(os.path.join(targetpath, target_category,
                    target_bed, target_bed + "_clean.bed"),
                    header = False, index = False, sep = "\t")


print("\n--------------------------------------\n")


## Mark beds

sel_marks = os.path.join(selpath, cell_type, marks_dataset)
for mark in os.listdir(sel_marks):

    print("Filtering: " + mark)

    # Read histone marks chip-seq
    mark_bed = os.listdir(os.path.join(sel_marks,mark))
    markbed_path = os.path.join(sel_marks, mark, " ".join(str(i) for i in mark_bed))
    mark_df = pd.read_csv(markbed_path, sep = "\t", header=None, compression='gzip',
              usecols = [0,1,2], names=['Chromosome', 'Start', 'End'])

    # Filter Dataframe
    mark_df = mark_df[mark_df['Chromosome'].isin(chr_list)]

    # Sort Dataframe
    mark_gr = pr.PyRanges(mark_df)
    mark_gr = mark_gr.sort()

    # Save filtered histone marks files
    mark_gr.to_csv(os.path.join(sel_marks, mark, mark + ".bed.gz"),
                   header = False, sep = "\t", compression='gzip')

    # Remove rest of files
    if len(os.listdir(os.path.join(sel_marks, mark))) == 2:
        for fnam in os.listdir(os.path.join(sel_marks, mark)):
            if fnam != str(mark + ".bed.gz"):
                os.remove(os.path.join(sel_marks, mark, fnam))

print("processing finished")
