import os
import sys
import math
import shutil
import pandas as pd
from paths import *
from ranges import * # LAD_ranges LAD_names

# This script is out from the general analysis pipeline, because it is used to generate the lamin bed files that the pipeline use to determinate the ranges in which the input chromatin bed will be divided in the nuclei.

LAD_cell = sys.argv[1] # H1-hESC
LAD_bed = sys.argv[2] # Lamin_B1
#norm = int(sys.argv[3]) # norm=100000


# Create dirs to save bed files for gd and hi-c analysis
#dir_lamin = os.path.join(ladspath, LAD_cell)
#source_lamin =  os.path.join(dir_lamin, "source", LAD_bed + ".bed")
source_lamin = os.path.join(ladspath, "source", LAD_cell, LAD_bed+".bed")


for dir in ['hi-c','gd']:
    #subdir_lamin = os.path.join(dir_lamin, dir, LAD_bed)
    subdir_lamin = os.path.join(ladspath, dir, LAD_cell, LAD_bed)
    if not os.path.exists(subdir_lamin):
        os.makedirs(subdir_lamin)
        shutil.copy(source_lamin, os.path.join(subdir_lamin,
                                               LAD_bed + ".InsideLAD.bed"))
## gd preparation
# Loop to generate new lamin beds
for dist in range(0, len(LAD_ranges)):
    if dist == 0:
        continue

    # Open original lamin bed
    fnam = pd.read_csv(source_lamin, sep = "\t", header=None,
                       names = ['Chromosome', 'Start', 'End'])

    # Extend the chromatin space occuped by lamin B1 by their ends according to the established ranges.
    fnam['Start'] = fnam['Start'] - LAD_ranges[dist]
    fnam['End'] = fnam['End'] + LAD_ranges[dist]
    fnam.loc[fnam.Start < 0, 'Start'] = 0

    nf = os.path.join(subdir_lamin, LAD_bed+"." +  LAD_names[dist]+ ".bed")

    # Save modified lamin bed
    fnam.to_csv(nf, header = False, sep = "\t", index = False)


## hi-c preparation

fnam = pd.read_csv(source_lamin, sep = "\t", header=None,
                   names = ['Chromosome', 'Start', 'End'])

# Convert genomic distances to mesure of bin hi-c
for col in ['Start', 'End']:
    fnam[col] = fnam[col].apply(lambda x: x / norm)
    fnam[col] = fnam[col].apply(lambda x: math.trunc(x))
    #hic_fnam = os.path.join(dir_lamin,'hi-c',LAD_bed, LAD_bed + ".InsideLAD.bed")
    hic_fnam = os.path.join(ladspath, 'hi-c', LAD_cell, LAD_bed, LAD_bed + ".InsideLAD.bed")
    fnam.to_csv(hic_fnam, header=False, index= False,  sep='\t')
