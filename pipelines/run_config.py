import os
import sys
import json
import argparse
from paths import *
from snakemake import snakemake


# Construct the argument parser
ap = argparse.ArgumentParser()

# Add the arguments to the parser
ap.add_argument("-c", "--config", required=True,
                help="config file with input parametters")

ap.add_argument("-s", "--smk", required=True,
                help="snakemake workflow")

args = ap.parse_args()

# Load Config data
if args.config and args.smk:
    input = os.path.join(configpath,args.config)
    with open(input,'r') as config_file:
        data = json.load(config_file)

        # Path to snakemake script
        smk = os.path.join(snakemakepath, args.smk)

        # Run snakemake rules and
        snk_status = snakemake(smk,
                     config = data,
                     lock = False, targets = ['statistics'],
                     cores = 10,
                     force_incomplete = True)
